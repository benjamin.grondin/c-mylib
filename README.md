# C - My lib.

To build `mylib`, run this command.

```.shell
make
```

---

## Functions availables

```.c
void my_putchar(char c);
void my_putnbr(int n);
void my_putstr(char *str);
char *my_strcat(char *s1, const char *s2);
int my_strcmp(const char *s1, const char *s2);
char *my_strcpy(char *dest, const char *src);
char *my_strdup(const char *s1);
int my_strlen(const char *str);
char *my_strncat(char *s1, const char *s2, size_t n);
int my_strncmp(const char *s1, const char *s2, size_t n);
char *my_strncpy(char *dest, const char *src, size_t n);
int my_tolower(int c);
int my_toupper(int c);
```

---

## Makefile

- `test`: Build and test code inside `main.c`
- `build`: Build librairie
- `clean`: Remove main binary if exist and all file *.o
- `clean_all`: Run command `make clean` and remove mylib.a