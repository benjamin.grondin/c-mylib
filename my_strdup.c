#include "mylib.h"

char *my_strdup(const char *s1)
{
    char *str = (char *)malloc(sizeof(const char) * my_strlen(s1) + 1);

    if (str != NULL)
    {
        size_t count = 0;
        while (s1[count])
        {
            str[count] = s1[count];
            count++;
        }

        str[count] = '\0';
    }

    return str;
}