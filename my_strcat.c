#include "mylib.h"

char *my_strcat(char *s1, const char *s2)
{
    if (s1 == NULL)
    {
        return NULL;
    }

    int count = 0;
    while (s1[count])
    {
        count++;
    }

    while (*s2)
    {
        s1[count] = *s2++;
        count++;
    }

    s1[count] = '\0';

    return s1;
}