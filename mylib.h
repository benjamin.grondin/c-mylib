#ifndef MYLIB_H
#define MYLIB_H

#include <unistd.h>
#include <stdlib.h>

void my_putchar(char c);
void my_putnbr(int n);
void my_putstr(char *str);
char *my_strcat(char *s1, const char *s2);
int my_strcmp(const char *s1, const char *s2);
char *my_strcpy(char *dest, const char *src);
char *my_strdup(const char *s1);
int my_strlen(const char *str);
char *my_strncat(char *s1, const char *s2, size_t n);
int my_strncmp(const char *s1, const char *s2, size_t n);
char *my_strncpy(char *dest, const char *src, size_t n);
int my_tolower(int c);
int my_toupper(int c);

#endif