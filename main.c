#include "mylib.h"

int main(int argc, char **argv)
{
    my_putchar(argv[0][0]);
    my_putchar('\n');
    my_putstr(argv[0]);
    my_putchar('\n');
    my_putnbr(argc);
    my_putchar('\n');
    my_putnbr(my_strlen(argv[0]));
    my_putchar('\n');

    char a[8];
    char *b = "un test";
    my_strcpy(a, b);
    my_putstr(a);
    my_putchar('\n');

    char c[8];
    my_strncpy(c, b, 5);
    my_putstr(c);
    my_putchar('\n');

    my_putnbr(my_strcmp("abc", "abe"));
    my_putchar('\n');
    my_putnbr(my_strncmp("abe", "abc", 1));
    my_putchar('\n');

    char d[20] = "cc ";
    my_strcat(d, "coucou");
    my_putstr(d);
    my_putchar('\n');

    char e[20] = "cc ";
    my_strncat(e, "coucou", 3);
    my_putstr(e);
    my_putchar('\n');

    char *f = my_strdup("coucou");
    my_putstr(f);
    my_putchar('\n');

    return 0;
}