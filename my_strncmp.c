#include "mylib.h"

int my_strncmp(const char *s1, const char *s2, size_t n)
{
    size_t count = 0;

    while (count < n && (*s1 && *s2) && *s1 == *s2)
    {
        s1++;
        s2++;
        count++;
    }

    return *s1 - *s2;
}