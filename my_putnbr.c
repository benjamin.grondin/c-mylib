#include "mylib.h"

void my_putnbr(int n)
{
	unsigned int nb;

	if (n < 0)
	{
		my_putchar('-');
		nb = -n;
	}
	else
		nb = n;
	if (nb > 9)
	{
		my_putnbr(nb / 10);
		nb %= 10;
	}
	my_putchar(nb + 48);
}