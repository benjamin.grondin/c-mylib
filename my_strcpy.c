#include "mylib.h"

char *my_strcpy(char *dest, const char *src)
{
    while (*src)
    {
        *dest++ = *src++;
    }

    *dest = '\0';

    return dest;
}
