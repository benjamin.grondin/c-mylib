DEFAULT: build

NAME = mylib.a

SRC = 	my_putchar.c \
		my_putnbr.c \
		my_putstr.c \
		my_strcat.c \
		my_strcmp.c \
		my_strcpy.c \
		my_strdup.c \
		my_strlen.c \
		my_strncat.c \
		my_strncmp.c \
		my_strncpy.c \
		my_tolower.c \
		my_toupper.c

OBJ = $(SRC:.c=.o)

build:
	@echo "Compiling..."
	@gcc -Wall -Wextra -Werror -c $(SRC) && ar rc $(NAME) $(OBJ)
	@echo "Done."

test:
	@gcc -Wall -Wextra -Werror *.c -o main
	./main

clean:
	@echo "Delete binary..."
	@/bin/rm -f main
	@echo "Delete *.o..."
	@/bin/rm -Rf $(OBJ)
	@echo "Done."

clean_all: clean
	@echo "Delete $(NAME)..."
	@/bin/rm -Rf $(NAME)
	@echo "Done."
