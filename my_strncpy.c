#include "mylib.h"

char *my_strncpy(char *dest, const char *src, size_t n)
{
    size_t count = 0;

    while (count < n)
    {
        if (*src)
        {
            *dest++ = *src++;
        }
        else
        {
            *dest++ = '\0';
        }
        count++;
    }

    return dest;
}
